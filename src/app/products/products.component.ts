import { Component, OnInit } from '@angular/core';
import {ProductsService} from './products.service';

@Component({
  selector: 'jce-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products;

deleteProduct(product){
  //  this.users.splice(
    //  this.users.indexOf(user),1
   // )
   this._productsServise.deleteproduct(product);//add user through service file where there is an add user function.       
  }

  constructor(private _productsServise: ProductsService) { }

  ngOnInit() {
    this._productsServise.getProducts()
    .subscribe(products => 
    {this.products = products});
  }

}