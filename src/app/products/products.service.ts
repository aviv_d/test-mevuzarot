import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';

@Injectable()
export class ProductsService {
  productsObservable;
  getProducts(){
     this.productsObservable = this.af.database.list('/products');
     return this.productsObservable;
  }
 deleteproduct(product){
let productKey = product.$key;
this.af.database.object('/products/' + productKey).remove();
    
  }
  constructor(private af:AngularFire) { }

}