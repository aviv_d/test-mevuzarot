import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import {Post} from '../post/post';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'jce-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  @Output() postAddedEvent = new EventEmitter<Post>();
  
  post:Post = {
    title: '',
    body: '',
    user: ''
  };
  //usr:User = {name:'',email:''};  
   //post:Post = {title: '', body: ''}
 
   onSubmit(form:NgForm){
    console.log(form);
    //Add a comment to this line
   this.postAddedEvent.emit(this.post);
   console.log(this.post)
    this.post = {
       title: '',
       body: '',
       user:''
    }
  }
constructor() { }
  ngOnInit() {
  }

}