import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService} from './users/users.service';
import { PostsService} from './posts/posts.service';
import { ProductsService} from './products/products.service';

import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostComponent } from './post/post.component';
import { PostFormComponent } from './post-form/post-form.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';

import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';
//import { UserFormComponent } from './user-form/user-form.component';

export const firebaseConfig = {
    apiKey: "AIzaSyDQhPi-QFg9Wfizlezf9R1z1SKjsp5LNQM",
    authDomain: "avivs-project.firebaseapp.com",
    databaseURL: "https://avivs-project.firebaseio.com",
    storageBucket: "avivs-project.appspot.com",
    messagingSenderId: "1092286053744"
 }

const appRoutes:Routes = [ //building routes according to url entered.
  {path:'invoiceForm',component:InvoiceFormComponent},
  {path:'invoices',component:InvoicesComponent},
  {path:'users',component:UsersComponent},//when relize path is users load user component.
  {path:'posts',component:PostsComponent},//when relize path is posts load post component.
  {path:'products',component:ProductsComponent},
  {path:'',component:InvoiceFormComponent},// when no path inserted load user component as default. 
  {path:'**',component:PageNotFoundComponent}//when path doesn't exist, load error component.
]


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostComponent,
    PostFormComponent,
    ProductsComponent,
    ProductComponent,
    InvoiceFormComponent,
    InvoicesComponent,
    InvoiceComponent,
   
 
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
     AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService,
               PostsService,
               ProductsService
               ],
  bootstrap: [AppComponent]
})
export class AppModule { }