import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import {Post} from './post'

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>();  //פה צריך להוסיף עבור ביטול עריכה 

 post:Post;
 //tempPost:Post = {title:null,body:null};
    //  @Output('title') tempTitle: string;
    //  @Output('body') tempBody: string;
 

 tempPost:Post = {title:null,body:null,user:null};

 isEdit : boolean = false;
  editButtonText = 'Edit';
  constructor() { }

/* cancelEdit(){                         // פונקציה המבטלת את העריכה כלומר שומרת את הערך הישן 
//Add a comment to this line
    this.isEdit = false;
    this.post.title = this.tempPost.title;
    this.post.body = this.tempPost.body;
    this.editButtonText = 'Edit'; 
  }
*/
sendDelete()
{
      this.deleteEvent.emit(this.post);
      
}

toggleEdit(){
     //update parent about the change
     
    //  this.temp = this.post.title;
    //  this.tempBody = this.post.body;
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit'; 

    //  if(!this.isEdit){
    //    this.editEvent.emit(this.post);




      if(this.isEdit){
       this.tempPost.title = this.post.title;
       this.tempPost.body = this.post.body;

       
     } else{

       this.editEvent.emit(this.post);
     }



    //  else {
    //   let originalAndNew = [];
    //    originalAndNew.push(this.tempPost,this.post);
    //    this.editEvent.emit(originalAndNew);
    //  }
     }
   


  //     if(this.isEdit){
  //      this.tempTitle = this.post.title;
  //      this.tempBody = this.post.body;
  //    } else {
  //      let originalAndNew = [];
  //      originalAndNew.push(this.tempPost,this.post);
  //    //Add a comment to this line
  //      this.editEvent.emit(originalAndNew);
  //    }

 // }

  ngOnInit() {
  }

}